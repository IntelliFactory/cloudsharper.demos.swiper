namespace Site

#nowarn "40"

open IntelliFactory.WebSharper

type Action = | Index

module Client =
    open IntelliFactory.WebSharper.JQuery

    type JQuery with
        [<Inline "$this.eq($i)">]
        member this.Eq (i: int) : JQuery = failwith "N/A"

    [<JavaScript>]
    let Initialize() =
        let inline ($) (s: string) = JQuery.Of s
        let inline JQ (s: Dom.Node) = JQuery.Of s

        let SetContentSize() =
            let wheight = JQ(As<Dom.Node> Html5.Window.Self).Height()
            (($) ".swiper-content")
                .Css("height", string (wheight - (($) ".swiper-nav").Height()))

        SetContentSize() |> ignore
        (($) "window").Resize(fun () -> SetContentSize()) |> ignore

        let rec contentSwiper =
            Swiper.Swiper(
                (($) ".swiper-content").Get(0),
                new Swiper.SwiperOptions(
                    OnSlideChangeStart = fun () -> UpdateNavPosition()
                ))

        and navSwiper =
            Swiper.Swiper(
                (($) ".swiper-nav").Get(0),
                new Swiper.SwiperOptions(
                    VisibilityFullFit = true,
                    SlidesPerView = "auto",
                    OnSlideClick =
                        fun () ->
                            contentSwiper.SwipeTo((navSwiper: Swiper.Swiper).ClickedSlideIndex, 500, false)))

        and UpdateNavPosition() =
            (($) ".swiper-nav .active-nav").RemoveClass "active-nav" |> ignore
            let activeNav = (($) ".swiper-nav .swiper-slide").Eq(contentSwiper.ActiveIndex).AddClass("active-nav")
            if activeNav.HasClass ".swiper-slide-visible" then
                let thumbsPerNav = System.Math.Floor(float navSwiper.Width / (float <| activeNav.Width()))-1.
                navSwiper.SwipeTo(activeNav.Index()-int thumbsPerNav, 500, false)
            else
                navSwiper.SwipeTo(activeNav.Index(), 500, false)
        ()

module Controls =
    open IntelliFactory.WebSharper.Html
    open IntelliFactory.WebSharper.Swiper

    type SwiperControl() =
        inherit Web.Control()

        [<JavaScript>]
        override this.Body =
            let contents =
                [
                    "img/1.jpg", "Imaginary talk", "Long long description here.."
                    "img/1.jpg", "Functional Mobile Applications in F#", "Built around F#'s language-oriented features, WebSharper brings powerful functional concepts and composable abstractions such as formlets, flowlets and sitelets to solve many of the challenging problems of web development, and offers a uniquely productive framework to build robust, client-oriented web and web-based mobile applications.

In this talk, I will demonstrate how WebSharper's F#-to-JavaScript translation can be extended to cover native mobile functionality available through JavaScript libraries across a variety of mobile platforms, enabling F# developers to author Android, iOS, Windows Phone applications with pure F# code. The resulting applications are stunningly short and concise, utilize powerful functional ideas and F# metaprogramming capabilities, and reach millions of devices through simple packaging into native mobile code."
                    "img/2.jpg", "Simile-Free Monad Recipes", "Most tutorials explain the whole monad burrito by first growing the wheat to make the tortilla. Metaphors can sometimes get in the way of the simple fact that most Haskell code boils down to five essential monads: IO, Reader, Writer, State and Error. The good news is they are easy to grok because the syntax is familiar and the semantics are already well understood by programmers of all stripes. This talk will focus simply on how to use them with no metaphors and simple code examples that do something useful. Attendees will leave the session knowing how to write real, useful apps in Haskell."
                    "img/3.jpg", "Functional Reactive Programming in the Netflix API", "The Netflix API receives over two billion requests a day from more than 800 different devices ranging from gaming consoles like the PS3, XBox and Wii to set-top boxes, TVs and mobile devices such as Android and iOS.

As the unique demands of different devices have have diverged it became clear that the API needed to allow optimizing client-server interaction (http://techblog.netflix.com/2013/01/optimizing-netflix-api.html). We achieve this by enabling the creation of customized service endpoints that reduce network chatter, leverage server-side processing power and decentralize the development of each endpoint so as to encourage and empower rapid innovation.

This presentation will describe how the Netflix API achieves these goals using functional reactive programming using RxJava in a polyglot Java stack. Highly concurrent service endpoints are implemented without blocking, synchronization or thread-safety concerns by using declarative functional reactive composition. Parallelism and resiliency are enabled by the underlying Hystrix fault tolerance isolation layer that fronts the dozens of distributed systems within the Netflix SOA."
                    "img/4.jpg", "Monads vs Macros", "The syntactic threading macros (-> and friends) hint at a whole new context for code that may be especially useful for network or other IO-related programs. Is it coincidence that the resulting code has a shape very much like state monad comprehension? We'll look at some code that can benefit from both approaches, weigh the benefits of each, and speculate about what could be done to make them better."
                    "img/5.jpg", "Functional composition", "Music theory is one of the most naturally elegant and functional domains. It's a perfect fit for Clojure, which is why the Overtone audio environment makes hacking sound so much fun.

Chris will start with the basic building block of sound, the sine wave, and will gradually accumulate abstractions culminating in a canon by Johann Sebastian Bach.

By the end you should agree that anyone who's a developer is a musician too - even if they don't know it yet."
                    "img/6.jpg", "Protocols, Functors and Type Classes", "We will be taking a whirl-wind tour of the functional landscape, by looking at the concept of polymorphism through the lens of Clojure, Ocaml, Haskell and finally, Scala. We will follow a single problem through all these different languages finally landing in the hybrid world of Scala to see how these ideas were mapped into an OO world. If you are considering what language to choose for your next project or you are a language nerd like myself this presentation has a little for everyone."
                    "img/7.jpg", "Living in a Post-Functional World", "\"Functional Programming\" is the modern received wisdom regarding program and language design. All the cool kids talk about writing code that is \"functional\" or even \"purely functional\". However, functional programming as it is traditionally defined really doesn't cut the mustard, and if you look at how modern functional architectures are being shaped, everyone seems to have tacitly arrived at the same conclusion: more is needed. In this talk, we will look at how modern functional languages like Scala, Clojure and even Haskell have evolved beyond the simple paradigms of the lambda calculus. We will see how the industry has really moved beyond functional programming and into the realm of something more powerful, more expressive and better suited to the task of large application architecture."
                    "img/8.jpg", "Copious Data, the \"Killer App\" for Functional Programming", "The world of Copious Data (permit me to avoid the overexposed term Big Data) is currently dominated by Apache Hadoop, a clean-room version of the MapReduce computing model and a distributed, (mostly) reliable file system invented at Google.

But the MapReduce computing model is hard to use. It's very course-grained and relatively inflexible. Translating many otherwise intuitive algorithms to MapReduce requires specialized expertise. The industry is already starting to look elsewhere...

However, the very name MapReduce tells us its roots, the core concepts of mapping and reducing familiar from Functional Programming. We'll discuss how to return MapReduce and Copious Data, in general, to its ideal place, rooted in FP. We'll discuss the core operations (\"combinators\") of FP that meet our requirements, finding the right granularity for modularity, myths of mutability and performance, and trends that are already moving us in the right direction. We'll see why the dominance of Java in Hadoop is harming progress.

You might think that concurrency is the \"killer app\" for FP and maybe you're right. I'll argue that Copious Data is just as important for driving FP into the mainstream. Actually, FP has a long tradition in data systems, but we've been calling it SQL..."
                ]
            Div [
                Div [Attr.Class "swiper-container swiper-content"] -< [
                    Div [Attr.Class "swiper-wrapper"] -<
                        (contents
                        |> Seq.map (fun (img, title, descr) ->
                                Div [Attr.Class "swiper-slide"] -< [
                                    Div [Attr.Class "inner"] -< [
                                        H1 [Text title]
                                        Img [Attr.Class "movie-pic"; Src img; Height "150"; Width "150"; Alt ""]
                                        Div [Attr.Class "movie-text"] -< [Text descr]
                                        Div [Attr.Class "clearfix"]
                                    ]
                                ]
                        ))
                ]
                Div [Attr.Class "swiper-container swiper-nav"] -< [
                    Div [Attr.Class "swiper-wrapper"] -<
                        (contents
                        |> Seq.map (fun (img, title, _) ->
                            Div [Attr.Class <| "swiper-slide" + if title = "Movie 1" then " active-nav" else ""] -< [
                                Span [Attr.Class "angle"]
                                Img [Src img; Alt ""]
                                Div [Attr.Class "title"] -< [Text title]
                            ]
                        ))
                ]
                |>! OnAfterRender (fun e ->
                    Client.Initialize()
                )
            ]
            :> _

module Skin =
    open System.Web
    open IntelliFactory.WebSharper.Sitelets

    let TemplateLoadFrequency =
        #if DEBUG
        Content.Template.PerRequest
        #else
        Content.Template.Once
        #endif

    type Page =
        {
            Title : string
            Body : list<Content.HtmlElement>
        }

    let MainTemplate =
        Content.Template<Page>("~/Main.html", TemplateLoadFrequency)
            .With("title", fun x -> x.Title)
            .With("body", fun x -> x.Body)

    let WithTemplate title body : Content<Action> =
        Content.WithTemplate MainTemplate <| fun context ->
            {
                Title = title
                Body = body context
            }
    
module Pages =
    open IntelliFactory.Html

    let IndexPage () =
        Skin.WithTemplate "Home" <| fun ctx ->
            [
                Div [new Controls.SwiperControl()]
            ]

module Site =
    open IntelliFactory.WebSharper.Sitelets

    let MySite =
        Sitelet.Infer (fun action ->
            match action with
            | Index ->
                Pages.IndexPage()
        )

open IntelliFactory.WebSharper.Sitelets

[<Sealed>]
type EmptyWebsite() =
    interface IWebsite<Action> with
        member this.Actions = [Action.Index]
        member this.Sitelet = Site.MySite

[<assembly: Website(typeof<EmptyWebsite>)>]
do ()

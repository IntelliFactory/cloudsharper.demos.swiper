namespace IntelliFactory.WebSharper.Swiper

module Definition =
    open IntelliFactory.WebSharper
    open IntelliFactory.WebSharper.InterfaceGenerator

    module Res =
//        let Js = Resource "Js" "https://raw.github.com/nolimits4web/Swiper/master/dist/idangerous.swiper-2.0.min.js"
        let Js = Resource "Js" "res/idangerous.swiper-2.0.min.js"
//        let Css = Resource "Css" "https://raw.github.com/nolimits4web/Swiper/master/dist/idangerous.swiper.css"
        let Css = Resource "Css" "res/idangerous.swiper.css"

    let Mode =
        Pattern.EnumStrings "mode" ["horizontal"; "vertical"]

    let SwiperOptions =
        Pattern.Config "SwiperOptions" {
            Required = []
            Optional =
                [
                    "speed",    T<int>
                    "autoplay", T<int>
                    "mode",     Mode.Type
                    "loop",     T<bool>
                    "visibilityFullFit",    T<bool>
                    "slidesPerView",        T<string>

                    "onSlideChangeStart",   T<unit> ^-> T<unit>
                    "onSlideClick",         T<unit> ^-> T<unit>
                ]
        }

    let Swiper =
        Class "Swiper"
        |+> [
            Constructor (T<Dom.Node> * !? SwiperOptions)
            Constructor (T<string> * !? SwiperOptions)
        ]
        |+> Protocol [
            "swipeNext"         => T<unit> ^-> T<unit>
            "swipePrev"         => T<unit> ^-> T<unit>
            "swipeTo"           => (T<int> * T<int> * T<bool>) ^-> T<unit>
            "browser.ie10"      => T<bool>
            "browser.ie8"       => T<bool>
            // ...
            "activeIndex"       =? T<int>
            // ...
            "width"             =? T<int>
            "height"            =? T<int>
            // ..
            "ClickedSlideIndex" =? T<int>
        ]
        |> Requires [Res.Css; Res.Js]

    let Assembly =
        Assembly [
            Namespace "IntelliFactory.WebSharper.Swiper.Resources" [
                Res.Css
                Res.Js
            ]
            Namespace "IntelliFactory.WebSharper.Swiper" [
                Mode
                SwiperOptions
                Swiper
            ]
        ]

open IntelliFactory.WebSharper.InterfaceGenerator

[<Sealed>]
type SwiperExtension() =
    interface IExtension with
        member ext.Assembly = Definition.Assembly

[<assembly: Extension(typeof<SwiperExtension>)>]
do ()
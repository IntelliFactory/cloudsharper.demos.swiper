#r "IntelliFactory.Build.dll"
#r "IntelliFactory.Core.dll"
#r "NuGet.Core.dll"

open IntelliFactory.Build

let bt = BuildTool().PackageId("Swiper", "0.1").Verbose()

let swiper =
    bt.WebSharper.Extension("Swiper")
        .Modules(["Main"])
        
let site =
    bt.WebSharper.HtmlWebsite("Site")
        .Modules(["Main"])
        .References(fun r ->
           [
              r.Project swiper
              r.Assembly "System.Web"
           ])
        
bt.Solution [
    swiper
    site
]
|> bt.Dispatch

